#pragma once

#include <utils/os/fs/Path.h>
#include <utils/storage/Array.h>

class Files
{
  public:
    /*!
     * @brief Load the whole content of a file into a byte array
     * @param path File to load
     * @param appendZeroTerminal nTrue to append a 0 at the end of the byte array
     * @return File content
     */
    static ByteArray LoadFile(const Path& path, bool appendZeroTerminal);

    /*!
     * @brief Load the whole content of a file into a string
     * @param path File to load
     * @return File content
     */
    static std::string LoadFile(const Path& path);

    /*!
     * @brief Load partial content of a file into a string
     * @param path File to load
     * @param from Offset to read from
     * @param size Size to read
     * @return Partial file content
     */
    static std::string LoadFile(const Path& path, long long from, long long size);

    /*!
     * @brief Save the given string into a file
     * @param path File path
     * @param content String ot save
     * @return True if the content has been saved
     */
    static bool SaveFile(const Path& path, const std::string& content);

    /*!
     * @brief Save the given byte array into a file
     * @param path File path
     * @param content String ot save
     * @return True if the content has been saved
     */
    static bool SaveFile(const Path& path, const ByteArray& content);

    /*!
     * @brief Append the given string at the end of the given file or create it if it does not exist
     * @param path File path
     * @param data Data buffer to append
     * @param size Data size to append
     * @return True if the content has been saved
     */
    static bool AppendToFile(const Path& path, const void* data, int size);

    /*!
     * @brief Append the given string at the end of the given file or create it if it does not exist
     * @param path File path
     * @param content String ot append
     * @return True if the content has been saved
     */
    static bool AppendToFile(const Path& path, const std::string& content)
    {
      return AppendToFile(path, content.c_str(), content.length());
    }

    /*!
     * @brief Check if the given file or internal resource actually exists
     * @param path Path to file or internal resource
     * @return True if the file or internal resource exists, false otherwise
     */
    static bool FileExists(const Path& path);
};

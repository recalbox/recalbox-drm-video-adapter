//
// Created by bkg2k on 05/10/2021.
//

#include <utils/os/fs/Path.h>
#include <utils/IniFile.h>
#include <utils/Strings.h>
#include <utils/Log.h>
#include "Boot.h"

Boot::Boot()
  : mAdapter(Adapter::None)
{
  // Loafd recalbox-boot.conf
  #ifdef DEBUG
  IniFile boot(Path("/recalbox/boot/recalbox-boot.conf"));
  #else
  IniFile boot(Path("/boot/recalbox-boot.conf"));
  #endif

  // Extract & split key
  std::string key = boot.AsString("kernel_video_adapter", "none");
  std::string value;
  Strings::SplitAt(key, ':', key, value, true);
  key = Strings::ToLowerASCII(key);

  // Record
  if      (key == "auto") { mAdapter = Adapter::Auto; }
  else if (key == "force")
  {
    mAdapter = Adapter::Force;
    mOptions = value;
    if (value.empty()) { LOG(LogError) << "[Boot] 'force' w/o parameter has no effect. Default to 'none'"; mAdapter = Adapter::None; }
  }
  else if (key != "none")
  {
    LOG(LogError) << "[Boot] Unreconized kernel_video_adapter value. Default to 'none'";
    mAdapter = Adapter::None;
  }
}

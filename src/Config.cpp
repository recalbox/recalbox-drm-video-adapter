//
// Created by bkg2k on 05/10/2021.
//

#include <utils/IniFile.h>
#include <utils/Strings.h>
#include <utils/Log.h>
#include "Config.h"

Config::Config()
  : mCvt{ { .Width = 0, .Height = 0, .Frequency = 0, .Interlaced = false, .ReducedBlanking = false, .Type = OutputType::HDMI },
          { .Width = 0, .Height = 0, .Frequency = 0, .Interlaced = false, .ReducedBlanking = false, .Type = OutputType::HDMI } }
{
  #ifdef DEBUG
  IniFile config(Path("/recalbox/boot/config.txt"));
  #else
  IniFile config(Path("/boot/config.txt"));
  #endif

  // Extract types
  OutputType t1 = config.AsInt("hdmi_drive", config.AsInt("hdmi_drive:0", 2)) == 1 ? OutputType::DVI : OutputType::HDMI;
  OutputType t2 = config.AsInt("hdmi_drive", config.AsInt("hdmi_drive:1", 2)) == 1 ? OutputType::DVI : OutputType::HDMI;

  // Extract cvt info
  ExtractCvt(mCvt[0], config.AsString("hdmi_cvt", config.AsString("hdmi_cvt:0")), t1);
  ExtractCvt(mCvt[1], config.AsString("hdmi_cvt", config.AsString("hdmi_cvt:1")), t2);

  // Check hdmi group and mode
  if ((config.AsInt("hdmi_group", config.AsInt("hdmi_group:0", 0)) != 2) ||
      (config.AsInt("hdmi_mode", config.AsInt("hdmi_mode:0", 0)) != 87)) mCvt[0].Invalidate();
  if ((config.AsInt("hdmi_group", config.AsInt("hdmi_group:1", 0)) != 2) ||
      (config.AsInt("hdmi_mode", config.AsInt("hdmi_mode:1", 0)) != 87)) mCvt[1].Invalidate();
}

void Config::ExtractCvt(Config::CVT& into, const std::string& from, OutputType type)
{
  // Split the cvt args
  std::vector<std::string> item = Strings::Split(from, ' ', true);
  if (item.size() != 7)
  {
    LOG(LogError) << "[Config] Invalid CVT configuration line.";
    return;
  }

  // Extract cvt args
  int width = 0, height = 0, freq = 0;
  bool interlaced = false, reducedblanking = false;
  if (Strings::ToInt(item[0], width))
    if (Strings::ToInt(item[1], height))
      if (Strings::ToInt(item[2], freq))
        if (Strings::ToBool(item[5], interlaced))
          if (Strings::ToBool(item[6], reducedblanking))
            into.Set(width, height, freq, interlaced, reducedblanking, type);
}

std::string Config::CVT::ToKernel(const std::string& outputName) const
{
  std::string result;
  result.append(outputName);
  result.append(1, ':');
  if (IsValid())
  {
    result.append(Strings::ToString(Width));
    result.append(1, 'x');
    result.append(Strings::ToString(Height));
    result.append(1, 'M');
    if (ReducedBlanking) result.append(1, 'R');
    result.append(1, '@');
    result.append(Strings::ToString(Frequency));
    if (Interlaced) result.append(1, 'i');
    if (Type == OutputType::DVI) result.append(1, 'D');
  }
  else result.append(1, 'd'); // disabled

  return result;
}

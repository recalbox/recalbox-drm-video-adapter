//
// Created by bkg2k on 05/10/2021.
//

#include "Boot.h"
#include "CmdLine.h"
#include "utils/Log.h"
#include "Config.h"

void Force(const Boot& boot)
{
  // Build forced params
  Strings::Vector params = Strings::Split(boot.Options(), ' ', true);
  for(int i = (int)params.size(); --i >= 0; )
    if (!Strings::StartsWith(params[i],LEGACY_STRING("video=")))
      params[i] = "video=" + params[i];

  // Load cmdline
  CmdLine cmd;

  // Check if they already exists
  if ((int)params.size() == cmd.VideoKeyCount())
  {
    bool equals = true;
    for(int i = cmd.VideoKeyCount(); --i >= 0; )
      equals &= (cmd.VideoKeyValue(i) == params[i]);
    if (equals)
    {
      LOG(LogInfo) << "[Main] Everything is already set, nothing to update.";
      return;
    }
  }

  // Update parameters
  for(int i = 0; i < (int)params.size(); i++)
    cmd.SetVideoValue(i, params[i]);
  if (cmd.Save())
  {
    LOG(LogInfo) << "[Main] /boot/cmdline.txt updated to: " << cmd.Content();
    #ifndef DEBUG
    system("reboot");
    #endif
  }
}

void Auto()
{
  // Config.txt + includes
  Config config;

  // Load cmdline
  CmdLine cmd;

  std::string cvt0 = "video=" + config.Cvt0().ToKernel("HDMI-A-1");
  std::string cvt1 = "video=" + config.Cvt1().ToKernel("HDMI-A-2");

  if (cmd.VideoKeyCount() == 2) // Always 2 video outputs configured
    if (cmd.VideoKeyValue(0) == cvt0 &&
        cmd.VideoKeyValue(1) == cvt1)
  {
    LOG(LogInfo) << "[Main] Everything is already set, nothing to update.";
    return;
  }

  cmd.SetVideoValue(0, cvt0);
  cmd.SetVideoValue(1, cvt1);
  if (cmd.Save())
  {
    LOG(LogInfo) << "[Main] /boot/cmdline.txt updated to: " << cmd.Content();
    #ifndef DEBUG
    system("reboot");
    #endif
  }
}

int main(int argc, char** argv)
{
  (void)argc;
  (void)argv;

  // Open logs
  Log::open();

  // Get boot options
  Boot boot;

  // Process
  switch(boot.Type())
  {
    case Boot::Adapter::None:
    {
      LOG(LogInfo) << "[Main] No video adaptation required.";
      break;
    }
    case Boot::Adapter::Auto:
    {
      LOG(LogInfo) << "[Main] Automatic video adaptation required.";
      Auto();
      break;
    }
    case Boot::Adapter::Force:
    {
      LOG(LogInfo) << "[Main] Forced video adaptation required.";
      Force(boot);
      break;
    }
    default: break;
  }

  return 0;
}

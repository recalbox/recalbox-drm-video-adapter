//
// Created by bkg2k on 05/10/2021.
//

#pragma once


#include <string>

class Config
{
  public:
    //! Output type
    enum class OutputType
    {
      HDMI, //!< HDMI, numeric video + sound
      DVI,  //!< DVI over HDMI, video only
    };

    //! CVT structure
    struct CVT
    {
      int Width;
      int Height;
      int Frequency;
      bool Interlaced;
      bool ReducedBlanking;
      OutputType Type;

      /*!
       * @brief Set structure
       * @param w Width
       * @param h Height
       * @param f Frequency
       * @param i Interlaced
       * @param r Reduced Blanking
       */
      void Set(int w, int h, int f, bool i, bool r, OutputType t)
      {
        Width = w;
        Height = h;
        Frequency = f;
        Interlaced = i;
        ReducedBlanking = r;
        Type = t;
      }
      //! Check if this structure is valid
      bool IsValid() const { return Width * Height * Frequency != 0; }
      //! Invalidate this structure
      void Invalidate() { Set(0, 0, 0, false, false, OutputType::HDMI); }
      /*!
       * @brief Convert this structure to Kernel config value
       * @param outputName Output name (ie: HDMI-A-1 for example)
       * @return Kernel compatible config
       */
      std::string ToKernel(const std::string& outputName) const;
    };

    //! Constructor
    Config();

    //! Access first output CVT
    const CVT& Cvt0() const { return mCvt[0]; }
    //! Access second output CVT
    const CVT& Cvt1() const { return mCvt[1]; }

  private:
    //! CVT definition
    CVT mCvt[2];

    /*!
     * @brief Extract CVT information into a CVT structure
     * @param into Target CVT structure
     * @param from Source string
     * @param type Output type
     */
    static void ExtractCvt(CVT& into, const std::string& from, OutputType type);
};




//
// Created by bkg2k on 05/10/2021.
//

#include <utils/Files.h>
#include <utils/math/Misc.h>
#include "CmdLine.h"

CmdLine::CmdLine()
#ifdef DEBUG
  : mCmdLinePath("/recalbox/boot/cmdline.txt")
#else
  : mCmdLinePath("/boot/cmdline.txt")
#endif
  , mVideoKeyCount(0)
{
  mContent = Strings::Split(Strings::Trim(Files::LoadFile(mCmdLinePath), " \t\r\n"), ' ', true);

  for(int i = (int)mContent.size(); --i >= 0; )
  {
    if (Strings::StartsWith(mContent[i],LEGACY_STRING("video="))) mVideoKeyCount++;
    else break;
  }
}

const std::string& CmdLine::VideoKeyValue(int index)
{
  index = Math::clampi(index, 0, mVideoKeyCount - 1);
  return mContent[(int)mContent.size() - mVideoKeyCount + index];
}

void CmdLine::SetVideoValue(int index, const std::string& value)
{
  index = Math::clampi(index, 0, mVideoKeyCount);
  if (index < mVideoKeyCount) mContent[(int)mContent.size() - mVideoKeyCount + index] = value;
  else { mContent.push_back(value); mVideoKeyCount++; }
}

bool CmdLine::Save()
{
  if (!Files::SaveFile(mCmdLinePath, Content()))
  {
    LOG(LogError) << "[CmdLine] Error saving " << mCmdLinePath.ToString() << " !";
    return false;
  }
  return true;
}

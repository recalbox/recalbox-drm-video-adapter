//
// Created by bkg2k on 05/10/2021.
//

#pragma once

#include <string>

class Boot
{
  public:
    //! Adapter type
    enum class Adapter
    {
      None,  //!< Nothing to do
      Auto,  //!< Auto adapt config.txt content into cmdline.txt
      Force, //!< Force given config into cmdline.txt
    };

    //! Constructor
    Boot();

    //! Get adapter
    Adapter Type() const { return mAdapter; }

    //! Get forced options
    const std::string& Options() const { return mOptions; }

  private:
    //! Adpater type
    Adapter mAdapter;
    //! Options
    std::string mOptions;
};




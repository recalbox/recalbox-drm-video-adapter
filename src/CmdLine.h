//
// Created by bkg2k on 05/10/2021.
//
#pragma once

#include <string>
#include "utils/os/fs/Path.h"
#include "utils/Strings.h"

class CmdLine
{
  public:
    //! Construtor
    CmdLine();

    //! Video component count (only **latest** to not interfer with rpi firmware)
    int VideoKeyCount() const { return mVideoKeyCount; }
    //! Video value from 0 to VideoKeyCount - 1
    const std::string& VideoKeyValue(int index);

    /*!
     * @brief Set video value at the given index. If the index is n+1 then the new value is added and
     * the total video key count increments
     * @param index Video value index
     * @param value Video value
     */
    void SetVideoValue(int index, const std::string& value);

    //! Get all content
    std::string Content() const { return Strings::Join(mContent, ' '); }

    //! Save content
    bool Save();

  private:
    //! Path
    Path mCmdLinePath;
    //! cmdline.txt content
    std::vector<std::string> mContent;
    //! Video key count
    int mVideoKeyCount;
};




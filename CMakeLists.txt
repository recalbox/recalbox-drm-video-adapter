cmake_minimum_required(VERSION 3.10)

project(drmadapter)

# default options
if (CMAKE_BUILD_TYPE MATCHES Debug)
    set(DRMADAPTER_OPTIMIZATIONS "-O0 -g3 -DDEBUG")
else()
    set(DRMADAPTER_OPTIMIZATIONS "-O3 -g0")
endif()

set(DRMADAPTER_DEFAULT_OPTIONS "-fPIC -Wall -Wextra -ffunction-sections -fdata-sections -Wl,--gc-sections -Wno-attributes -Wswitch ${DRMADAPTER_DEFAULT_DEFINES} ${DRMADAPTER_OPTIMIZATIONS}")

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${DRMADAPTER_DEFAULT_OPTIONS}")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${DRMADAPTER_DEFAULT_OPTIONS}")
set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} ${DRMADAPTER_DEFAULT_OPTIONS}")

# Add all
file(GLOB DRMADAPTER_HEADERS_ORG src/*.h)
file(GLOB DRMADAPTER_SOURCES_ORG src/*.cpp)
file(GLOB_RECURSE DRMADAPTER_HEADERS_RECALBOX src/utils/*.h)
file(GLOB_RECURSE DRMADAPTER_SOURCES_RECALBOX src/utils/*.cpp)

LIST(APPEND DRMADAPTER_HEADERS ${DRMADAPTER_HEADERS_ORG} ${DRMADAPTER_HEADERS_RECALBOX})
LIST(APPEND DRMADAPTER_SOURCES ${DRMADAPTER_SOURCES_ORG} ${DRMADAPTER_SOURCES_RECALBOX})

set(DRMADAPTER_LIBRARIES "")

# Final target
add_link_options($<$<CONFIG:RELEASE>:-s>)
include_directories(src)
add_executable(drmadaper ${DRMADAPTER_SOURCES} ${DRMADAPTER_HEADERS})
target_link_libraries(drmadaper pthread SDL2 ${DRMADAPTER_LIBRARIES})

message(
        "=========================================================================================================\n"
        "\n"
        "C    Flags : ${CMAKE_C_FLAGS}\n"
        "C++  Flags : ${CMAKE_CXX_FLAGS}\n"
        "Link Flags : ${CMAKE_EXE_LINKER_FLAGS}\n"
        "Source     ! ${DRMADAPTER_SOURCES}\n"
        "\n"
        "=========================================================================================================\n"
)

install(TARGETS drmadaper
        RUNTIME
        DESTINATION bin)

INCLUDE(InstallRequiredSystemLibraries)

SET(CPACK_PACKAGE_DESCRIPTION_SUMMARY "A DRM Video adapter by the Recalbox Team")
SET(CPACK_PACKAGE_DESCRIPTION "Report config.txt configuration into cmdline.txt for use by the kernel.")

SET(CPACK_RESOURCE_FILE LICENSE "LICENSE.md")
SET(CPACK_RESOURCE_FILE README "README.md")

SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Bkg2k <bkg2k@recalbox.com>")
SET(CPACK_DEBIAN_PACKAGE_SECTION "misc")
SET(CPACK_DEBIAN_PACKAGE_PRIORITY "extra")

SET(CPACK_PACKAGE_VENDOR "recalbox.com")
SET(CPACK_PACKAGE_VERSION "2.0.0~rc1")
SET(CPACK_PACKAGE_VERSION_MAJOR "2")
SET(CPACK_PACKAGE_VERSION_MINOR "0")
SET(CPACK_PACKAGE_VERSION_PATCH "0")
SET(CPACK_PACKAGE_INSTALL_DIRECTORY "DRMADAPTER_${CMAKE_PACKAGE_VERSION}")
SET(CPACK_PACKAGE_EXECUTABLES "drmadaper" "drmadaper")

SET(CPACK_GENERATOR "TGZ;DEB")

INCLUDE(CPack)